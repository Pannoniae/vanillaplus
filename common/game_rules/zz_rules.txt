historical_mode = {
    name = "RULE_HISTORICAL_MODE"
    group = "RULE_GROUP_GAMEPLAY"
    icon = "GFX_wargoals"
    option = {
        name = no
        text = "RULE_OPTION_NORMAL"
        desc = "RULE_HISTORICAL_MODE_NORMAL_DESC"
        allow_achievements = yes
    }
    option = {
        name = yes
        text = "RULE_OPTION_HISTORICAL"
        desc = "RULE_HISTORICAL_MODE_HISTORICAL_DESC"
        allow_achievements = yes
    }
}