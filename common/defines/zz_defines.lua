NDefines.NMilitary.MAX_ARMY_EXPERIENCE = 5000			--Max army experience a country can store
NDefines.NMilitary.MAX_NAVY_EXPERIENCE = 5000			--Max navy experience a country can store
NDefines.NMilitary.MAX_AIR_EXPERIENCE = 5000				--Max air experience a country can store

NDefines.NMilitary.BASE_DIVISION_BRIGADE_GROUP_COST = 0 	--Base cost to unlock a regiment slot,
NDefines.NMilitary.BASE_DIVISION_BRIGADE_CHANGE_COST = 0	--Base cost to change a regiment column.
NDefines.NMilitary.BASE_DIVISION_SUPPORT_SLOT_COST = 0 	--Base cost to unlock a support slot

NDefines.NMilitary.BASE_COMBAT_WIDTH = 96                       -- base combat width
NDefines.NMilitary.ADDITIONAL_COMBAT_WIDTH = 48