﻿name="Vanilla"
path="mod/vanillaplus"
tags={
	"Fixes"
    "Balance"
    "Gameplay"
    "Historical"
    "Map"
    "National Focuses"
}
supported_version="1.7.1"
